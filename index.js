var app = require('express')();

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var http = require('http').Server(app);
var io = require('socket.io')(http);


var config = require('./config.json');
var remote = require('./lib/remote');

if (!config) {
  console.error('Unable to read config');
  process.exit(3);
}

app.get('/', function(req, res) {
  res.send('<h1>Hello ' + config.app.name + '</h1>');
});

app.post('/api/v1/notify', function(req, res) {


  if(req.body && req.body.pin && req.body.phone &&  req.body.data)  {
    var pid = req.body.phone+':'+req.body.pin;
    if(typeof(_pids[pid]) != 'undefined') {
      console.log('Notifing ' + req.body.phone);
      _pids[pid].notify(req.body.data);
    } else {
      console.log('No client for ' + pid);
    }
  }

  res.json({res: 'OK', code: 200});

});

var _pids = {};

io.on('connection', function(socket) {
  console.log('a peer connected');

  var _remote = remote(socket);

  socket.on('error', function() {
    delete _pids[_remote.pid];
  })

  socket.on('waitfor', function(data) {

    var pid = data.phone + ':' + data.pin;

    console.log('waits for ' + pid);
    _remote.pid = pid;
    _pids[pid] = _remote;
  })

  socket.on('disconnect', function() {
    delete _pids[_remote.pid];
  })

});

http.on('listening', function() {
  console.log('Express server started on port %s at %s', http.address().port, http.address().address);
});

http.listen(config.http.port, config.http.bind);
